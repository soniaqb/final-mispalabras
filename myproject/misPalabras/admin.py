from django.contrib import admin
from .models import Word, Comment, Voto, Usuario, Info, Enlace

#from django.contrib.auth.admin import UserAdmin



admin.site.register(Usuario)
admin.site.register(Word)
admin.site.register(Comment)
admin.site.register(Voto)
admin.site.register(Info)
admin.site.register(Enlace)
