import json
import urllib
from urllib.error import HTTPError
from urllib.parse import quote
from xml.dom.minidom import parse

from django.contrib.auth import logout, login, authenticate
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt

from .models import Word, Comment, Voto, Usuario, Info, Enlace
from .clases import *
from django.contrib.auth.forms import AuthenticationForm
from django.core.paginator import Paginator
import random
from django.utils import timezone



@csrf_exempt
def index(request):
    # lista de palabras ordenadas por fecha
    lista = Word.objects.all().order_by('-date')
    # lista de palabras ordenadas por votos
    listvoto = Word.objects.all().order_by('-contador')[0:5]

    if (request.method == 'GET') and ('format' in request.GET):
        formato = request.GET.get('format')
        if formato == 'xml':
            response = render(request, 'contenidos.xml', {'Lista': lista}, content_type='text/xml')
        if formato == 'json':
            response = render(request, 'contenidos.json', {'Lista': lista}, content_type='text/json')
        return response

    # cogeremos una palabra al azar
    pal_random = None
    if len(lista) != 0:
        num_random = random.randint(0, len(lista)-1)
        pal_random = lista[num_random]

    # Administra datos divididos en varias paginas
    paginator = Paginator(lista, 5)

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    if (request.method == 'GET') or (request.method == 'POST'):
        # Empezaremos con la autentificación
        forme = AuthenticationForm()
        form = request.POST.get('content')
        if form is not None:
            return HttpResponseRedirect('/'+str(form))
        else:
            context = {
                'Lista': lista,
                'login': forme,
                'word': '',
                'listvoto': listvoto,
                'numpalabras': len(lista),
                'palabra': pal_random,
                'page_obj': page_obj,
            }
            response = render(request, 'pages/formular_pal.html', context)

        return HttpResponse(response)

def wiki(request, word):
    try:
        texto = "https://es.wikipedia.org/w/api.php?action=query&format=xml&titles="+str(quote(word))+"&prop=extracts&exintro&explaintext"
        xml = parse(urllib.request.urlopen(texto))
        extract = (xml.getElementsByTagName('extract')).item(0)
        defi = extract.firstChild.nodeValue
    except AttributeError:
        defi = ('No se ha encontrado definición')

    try:
        imagen = "https://es.wikipedia.org/w/api.php?action=query&titles="+str(quote(word))+"&prop=pageimages&format=json&pithumbsize=200"
        json_str = urllib.request.urlopen(imagen)
        json_doc = json_str.read().decode(encoding="ISO-8859-1")
        img = json.loads(json_doc)
        for key in img['query']['pages'].keys():
            id = key
        url_imagen = img['query']['pages'][id]['thumbnail']['source']
        #open_img = urlretrieve(url_imagen,'mispalabras/templates/pages/imagen.jpg')
    except KeyError:
        url_imagen = ''
    return (defi,url_imagen)



def flickr(request,word):
    if (request.method == 'POST') and ('flickr' in request.POST):
        try:
            url = "https://www.flickr.com/services/feeds/photos_public.gne?tags=" + str(quote(word))
            xml = parse(urllib.request.urlopen(url))
            feed = xml.getElementsByTagName('feed')
            entry = feed[0].getElementsByTagName('entry')
            link = entry[0].getElementsByTagName('link')
            for tag in link:
                atributo = tag.getAttribute('rel')
                if atributo == 'enclosure':
                    href = tag.getAttribute('href')

        except AttributeError:
            href = ''
        except IndexError:
            href = ''
        i = Info(web='flickr')
        i.img = href
        i.usuario = Usuario.objects.get(usuario=request.user)
        i.palabra = Word.objects.get(word=word)
        i.date = timezone.now()
        i.save()
    else:
        try:
            href = Info.objects.get(web='flickr',palabra=word).img
        except Info.DoesNotExist:
            href = None
    return href

def gestion_voto(request, word):
    try:
        p = Word.objects.get(word=word)
        if (request.method == 'POST') and ('voto' in request.POST):
            try:
                v = Voto.objects.get(palabra=word, usuario=Usuario.objects.get(usuario=request.user))
                if v.votado:
                    voto = 'Votar palabra'
                    v.votado = False
                else:
                    voto = 'Quitar voto'
                    v.votado = True
            except Voto.DoesNotExist:
                voto = 'Quitar voto'
                v = Voto()
                v.votado = True
                v.palabra = p
                v.usuario = Usuario.objects.get(usuario=request.user)
                v.date = timezone.now()
            v.save()
            p.contador = len(Voto.objects.filter(votado=True,palabra=word))
            p.save()
        else:
            try:
                v = Voto.objects.get(palabra=word, usuario=Usuario.objects.get(usuario=request.user))
                if v.votado:
                    voto = 'Quitar voto'
                else:
                    voto = 'Votar palabra'
            except Voto.DoesNotExist:
                voto = 'Votar palabra'
    except Word.DoesNotExist:
        voto=''
    return voto


def gestion_enlace(request,word):
    if request.method == 'POST' and 'btnenlace' in request.POST:
        try:
            form = request.POST['enlace']
            user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
            headers = {'User-Agent': user_agent}
            req = urllib.request.Request(form, headers=headers)
            response = urllib.request.urlopen(req)
            html = response.read().decode('utf8')
            parserdefi = ParserDefi()
            parserimg = ParserImg()
            parserdefi.feed(html)
            parserimg.feed(html)
            definicion = [parserdefi.defi, parserimg.img]
            r = Enlace(url=form)
            r.defi = definicion[0]
            r.palabra = Word.objects.get(word=word)
            r.img = definicion[1]
            r.usuario = Usuario.objects.get(usuario=request.user)
            r.date = timezone.now()
            r.save()
        except UnicodeError:
            r = Enlace(url=form)
            r.defi = ''
            r.palabra = Word.objects.get(word=word)
            r.img = ''
            r.usuario = Usuario.objects.get(usuario=request.user)
            r.date = timezone.now()
            r.save()
        except HTTPError:
            form = request.POST['enlace']
            response = urllib.request.urlopen(form)
            html = response.read().decode('utf8')
            parserdefi = ParserDefi()
            parserimg = ParserImg()
            parserdefi.feed(html)
            parserimg.feed(html)
            definicion = [parserdefi.defi, parserimg.img]
            r = Enlace(url=form)
            r.defi = definicion[0]
            r.palabra = Word.objects.get(word=word)
            r.img = definicion[1]
            r.usuario = Usuario.objects.get(usuario=request.user)
            r.date = timezone.now()
            r.save()
    try:
        info = Enlace.objects.filter(palabra=word)
    except Enlace.DoesNotExist:
        info = None
    return info

def palabra(request,word):
    # devuelve la cadena en minusculas
    word = word.lower()

    try:
        if (request.method == 'GET') or (request.method == 'POST'):
            try:
                p = Word.objects.get(word=word)
                defi = (p.defi,p.img)
                guarda = None
                if 'comment' in request.POST:
                    p = Word.objects.get(word=word)
                    form = ContentForm(request.POST)
                    if form.is_valid():
                        c = Comment(comentario=form.cleaned_data['content'])
                        c.palabra = p
                        c.usuario = Usuario.objects.get(usuario=request.user)
                        c.date = timezone.now()
                        c.save()
            except Word.DoesNotExist:
                defi = wiki(request,word)
                if request.method == 'GET':
                    guarda = 'Guardar palabra'
                elif request.method == 'POST':
                    try:
                        p = Word.objects.get(word=word)
                        guarda = None
                    except Word.DoesNotExist:
                        if 'guarda' in request.POST:
                            p = Word(word=word)
                            p.defi = defi[0]
                            p.img = defi[1]
                            p.usuario = Usuario.objects.get(usuario=request.user)
                            p.date = timezone.now()
                            p.save()
                            guarda = None
                        else:
                            guarda = 'Guardar palabra'
            imgfli = flickr(request, word)
            comentarios = Comment.objects.filter(palabra=word)
            voto = gestion_voto(request, word)
            try:
                infrae = Info.objects.get(web='rae',palabra=word)
                drae = [infrae.defi,infrae.img]
            except Info.DoesNotExist:
                if request.method == 'POST' and 'rae' in request.POST:
                    url = 'https://dle.rae.es/' + str(quote(word))
                    user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
                    headers = {'User-Agent': user_agent}
                    req = urllib.request.Request(url, headers=headers)
                    parserdefi = ParserDefi()
                    parserimg = ParserImg()
                    with urllib.request.urlopen(req) as response:
                        html = response.read().decode('utf8')
                        parserdefi.feed(html)
                        parserimg.feed(html)
                        drae = [parserdefi.defi, parserimg.img]
                    r = Info(web='rae')
                    r.defi = drae[0]
                    r.palabra = Word.objects.get(word=word)
                    r.img = drae[1]
                    r.usuario = Usuario.objects.get(usuario=request.user)
                    r.date = timezone.now()
                    r.save()
                else:
                    drae = [None, None]

            try:
                cuenta = Word.objects.get(word=word)
                veces = cuenta.contador
            except  Word.DoesNotExist:
                veces = 0
            comment = ContentForm()
            enlace = UrlForm()
            tarjeta = gestion_enlace(request,word)
            lista = Word.objects.all().order_by('-date')
            listvoto = Word.objects.all().order_by('-contador')[0:5]
            try:
                palabra = [len(lista),random.choice(lista).word]
            except IndexError:
                palabra = [0,None]
            forme = UserForm()
            if request.method == 'POST' and 'apimeme' in request.POST:
                form = TextForm(request.POST)
                if form.is_valid():
                    texto = form.cleaned_data['text']
                    option = form.cleaned_data['option']
                    if option == '1':
                        imgapi = "http://apimeme.com/meme?meme=Advice-Yoda&top=" + str(quote(word)) + "&bottom=" + str(
                            quote(texto))
                    elif option == '2':
                        imgapi = "http://apimeme.com/meme?meme=Ancient-Aliens&top=" + str(quote(word)) + "&bottom=" + str(
                            quote(texto))
                    elif option == '3':
                        imgapi = "http://apimeme.com/meme?meme=Afraid-To-Ask-Andy&top=" + str(
                            quote(word)) + "&bottom=" + str(quote(texto))

                i = Info(web='apimeme')
                i.img = imgapi
                i.usuario = Usuario.objects.get(usuario=request.user)
                i.palabra = Word.objects.get(word=word)
                i.date = timezone.now()
                i.save()
            else:
                try:
                    imgapi = Info.objects.get(web='apimeme', palabra=word).img
                except Info.DoesNotExist:
                    imgapi = None
            texto = TextForm()
            context = {
                'login': forme,
                'word': word,
                'defi': defi[0],
                'imagen': defi[1],
                'voto': voto,
                'veces': veces,
                'guarda': guarda,
                'form': comment,
                'comentarios': comentarios,
                'drae': drae[0],
                'draeimg': drae[1],
                'numpalabras': palabra[0],
                'palabra': palabra[1],
                'imgfli': imgfli,
                'texto': texto,
                'imgapi': imgapi,
                'enlace': enlace,
                'tarjeta': tarjeta,
                'listvoto': listvoto,
            }
            content_re = render(request, 'pages/palabra.html', context)
            response = HttpResponse(content_re)
    except TypeError:
        if request.method == 'GET' or request.method == 'POST':
            try:
                p = Word.objects.get(word=word)
                defi = (p.defi,p.img)
            except Word.DoesNotExist:
                defi = wiki(request,word)

            lista = Word.objects.all().order_by('-date')
            listvoto = Word.objects.all().order_by('-contador')[0:5]
            try:
                palabra = [len(lista), random.choice(lista).word]
            except IndexError:
                palabra = [0, None]
            forme = UserForm()
            context = {
                'word': word,
                'defi': defi[0],
                'imagen': defi[1],
                'login': forme,
                'numpalabras': palabra[0],
                'palabra': palabra[1],
                'listvoto': listvoto,
            }
            content_re = render(request, 'pages/palabra.html', context)
            response = HttpResponse(content_re)
    return (response)



def entrar(request):
    if request.method == "POST":
        form = AuthenticationForm(request=request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return HttpResponseRedirect('/')
            else:
                form = AuthenticationForm()
        else:
            form = AuthenticationForm()
        message = 'Nombre de usuario o contraseña inválidos'
    else:
        form = AuthenticationForm()
        message = ''
    lista = Word.objects.all().order_by('-date')
    listvoto = Word.objects.all().order_by('-contador')[0:5]
    try:
        palabra = [len(lista), random.choice(lista).word]
    except IndexError:
        palabra = [0, None]

    context = {
        'message': message,
       'login': form,
       'listvoto': listvoto,
       'numpalabras': palabra[0],
       'palabra': palabra[1]
       }
    return HttpResponse(render(request, 'registration/login.html', context))



def salir(request):
    logout(request)
    return HttpResponseRedirect("/")



def registro(request):
    if request.method == "POST":
        form = UserForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.set_password(user.password)
            user.save()
            u = Usuario(usuario=user)
            u.save()
            login(request, user)
            return HttpResponseRedirect('/')
        form = UserForm()
        message = 'Nombre de usuario inválido: Solo se permiten 150 caracteres o menos. Letras, números y @/./+/-/_ only. ' \
                  'Puede que el nombre no esté disponible'
    else:
        form = UserForm()
        message =''
    lista = Word.objects.all().order_by('-date')
    listvoto = Word.objects.all().order_by('-contador')[0:5]
    try:
        palabra = [len(lista), random.choice(lista).word]
    except IndexError:
        palabra = [0, None]
    paginator = Paginator(lista, 5)

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    context = {
        'register_form':form,
        'message': message,
        'listvoto': listvoto,
        'numpalabras': palabra[0],
        'palabra': palabra[1],
        'Lista': lista,
        'page_obj': page_obj
    }
    return HttpResponse(render(request, 'pages/index.html', context))



def mipagina(request):
    if request.method == 'GET':
        lista = Word.objects.all().order_by('-date')
        listvoto = Word.objects.all().order_by('-contador')[0:5]
        try:
            palabra = [len(lista), random.choice(lista).word]
        except IndexError:
            palabra = [0, None]
        try:
            forme = AuthenticationForm()
            user = Usuario.objects.get(usuario=request.user)
            palabras = Word.objects.filter(usuario=user).order_by('-date')
            tp = Word()
            comentarios = Comment.objects.filter(usuario=user).order_by('-date')
            tc = Comment
            infos = Info.objects.filter(usuario=user).order_by('-date')
            ti = Info()
            enlaces = Enlace.objects.filter(usuario=user).order_by('-date')
            te = Enlace()
            todo = []
            for item in palabras:
                todo.append((item,item.date))
            for item in comentarios:
                todo.append((item,item.date))
            for item in infos:
                todo.append((item,item.date))
            for item in enlaces:
                todo.append((item,item.date))
            todo.reverse()
            final = []
            for item in todo:
                if (type(item[0])) == type(tp):
                    final.append((item[0],'1'))
                elif type(item[0]) == type(tc):
                    final.append((item[0],'2'))
                elif type(item[0]) == type(ti):
                    final.append((item[0],'3'))
                elif type(item[0]) == type(te):
                    final.append((item[0],'4'))
            context = {
                'login': forme,
                'orden': final,
                'numpalabras': palabra[0],
                'palabra': palabra[1],
                'listvoto': listvoto,
            }
            return HttpResponse(render(request, 'pages/mipagina.html', context))
        except TypeError:
            forme = AuthenticationForm()
            lista = Word.objects.all().order_by('-date')
            listvoto = Word.objects.all().order_by('-contador')[0:5]
            try:
                palabra = [len(lista), random.choice(lista).word]
            except IndexError:
                palabra = [0, None]
            mensaje = 'Debes estar logueado para ver esta información'
            context = {
                'login': forme,
                'mensaje': mensaje,
                'numpalabras': palabra[0],
                'palabra': palabra[1],
                'listvoto': listvoto,
            }
            return HttpResponse(render(request, 'pages/mipagina.html', context))

def ayuda(request):
    if request.method == 'GET' or request.method == 'POST':
        lista = Word.objects.all().order_by('-date')
        listvoto = Word.objects.all().order_by('-contador')[0:5]
        try:
            palabra = [len(lista), random.choice(lista).word]
        except IndexError:
            palabra = [0, None]
        forme = AuthenticationForm()
        form = request.POST.get('content')
        if form is not None:
            return HttpResponseRedirect('/'+str(form))
        else:
            context = {
                'login': forme,
                'listvoto': listvoto,
                'numpalabras': palabra[0],
                'palabra': palabra[1]
            }
            response = render(request, 'pages/ayuda.html', context)
        return HttpResponse(response)
