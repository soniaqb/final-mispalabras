from html.parser import HTMLParser

from django import forms
from django.contrib.auth.models import User

class ParserDefi(HTMLParser):
    def __init__(self):
        super().__init__()
        self.defi = ''
        self.val = ''
        self.sel = False
        self.atr =[]
        self.select = False

    def handle_starttag(self, tag, attrs):
        if tag == 'meta':
            self.atr.append(('Final', 'Inicio'))
            for atr in attrs:
                self.atr.append(atr)
            for at in self.atr:
                if at[0] == 'Final' and at[1] == 'Inicio':
                    self.val = ''
                    self.select = False
                    self.sel = False
                if self.select:
                    if at[0] == 'content':
                        self.defi = at[1]
                        self.select = False
                        break
                if at[0] == 'property' and at[1] == 'og:description':
                    self.select = True
                    if self.sel:
                        self.defi = self.val
                        break
                if at[0] == 'content':
                    self.sel = True
                    self.val = at[1]


class ParserImg(HTMLParser):
    def __init__(self):
        super().__init__()
        self.val = ''
        self.sel = False
        self.img = ''
        self.atr =[]
        self.sel_img = False


    def handle_starttag(self, tag, attrs):
        if tag == 'meta':
            self.atr.append(('Final', 'Inicio'))
            for atr in attrs:
                self.atr.append(atr)
            for at in self.atr:
                if at[0] == 'Final' and at[1] == 'Inicio':
                    self.val = ''
                    self.sel_img = False
                    self.sel = False
                if self.sel_img:
                    if at[0] == 'content':
                        self.img = at[1]
                        self.sel_img = False
                        break
                if at[0] == 'property' and at[1] == 'og:image':
                    self.sel_img = True
                    if self.sel:
                        self.img = self.val
                        break
                if at[0] == 'content':
                    self.sel = True
                    self.val = at[1]

class ContentForm(forms.Form):
    content = forms.CharField(label=False, widget=forms.Textarea(attrs={'rows':'2', 'cols': '105'}), max_length=50)

class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'password')
        help_texts = {
            'username': None
        }
        widgets = {
           'password': forms.PasswordInput()
        }

class TextForm(forms.Form):
    text = forms.CharField(label=False, widget=forms.Textarea(attrs={'rows': '1'}), max_length=50)
    choices = [(1,'yoda'),(2,'alien'),(3,'andy')]
    option = forms.ChoiceField(label=False,choices=choices)


class UrlForm(forms.Form):
    enlace = forms.URLField(label=False, widget=forms.URLInput(attrs={'rows':'1'}), max_length=256)


