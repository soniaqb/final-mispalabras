# Final MisPalabras

Práctica final curso 2021-2022 (MisPalabras)
# Entrega practica


## Datos
* Nombre: Sonia Quevedo Becerra
* Titulación: Grado en Ingeniería Telemática
* Despliegue (url): http://soniaqb.pythonanywhere.com/
* Video básico (url): https://www.youtube.com/watch?v=Krw8irmdfx0


## Cuentas usuarios
* carlos/carlos
* sonia/PRUEBA
* pepito/Sonia

## Resumen parte obligatoria
La parte obligatoria creo que esta completa, 
el único fallo que observo es que en el despliegue 
me falla una librería al buscar alguna palabra, pero 
si lo hago desde el local me funciona perfectamente.
Por motivos de tiempo no he podido arreglar ese fallo.

El vídeo tiene una duración de 3:05 min.
## Lista partes opcionales
* Nombre parte: favicon
